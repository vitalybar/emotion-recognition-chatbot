import src

RAW_NEGATIVE_DATA_PATH = r"data\raw\negative.csv"
RAW_POSITIVE_DATA_PATH = r"data\raw\positive.csv"
RAW_FEATURES_PATH = r"data\interim\features.csv"
LABELS_DATA_PATH = r"data\processed\labels.csv"
PROCESSED_FEATURES_PATH = r"data\processed\processed_features.csv"

if __name__ == "__main__":
    src.feature_preparing(
        RAW_POSITIVE_DATA_PATH,
        RAW_NEGATIVE_DATA_PATH,
        RAW_FEATURES_PATH,
        LABELS_DATA_PATH,
    )
    src.feature_processing(RAW_FEATURES_PATH, PROCESSED_FEATURES_PATH)

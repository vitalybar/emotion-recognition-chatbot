from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description='Emotional coloring of the text to assess the concentration of negative messages in business chats.',
    author='Mikhaylov Alexey',
    license='',
)

import re
import pandas as pd


class DataProcessing:
    """
    The class performs data preprocessing
    """

    def __init__(self, data_path):
        self.data_path = data_path

    def data_preparing(self, positive_data_csv: str,
                     negative_data_csv: str) -> list:
        """
        The method receives and performs data preprocessing
        :param str positive_data_csv: path to data file with positive messages
        :param str negative_data_csv: path to data file with negative messages
        :return: prepared pandas DataFrame
        :rtype:  pd.DataFrame
        """
        data1 = pd.read_csv(positive_data_csv, delimiter=';',
                            header=None, nrows=100000)
        data1 = data1[[3, 4]].rename({3: "x", 4: "y"}, axis='columns')
        data2 = pd.read_csv(negative_data_csv, delimiter=';',
                            header=None, nrows=100000)
        data2 = data2[[3, 4]].rename({3: "x", 4: "y"}, axis='columns')
        data = pd.concat([data1, data2])
        data = data[['x', 'y']]
        features = data['x'].values
        labels = data['y'].astype('int').values
        return [features, labels]


    def data(self, features, labels, catboost: bool):
        processed_features = []
        for sentence in range(0, len(features)):
            # Remove all the special characters
            processed_feature = re.sub(r'\W', ' ', str(features[sentence]))

            # remove all single characters
            processed_feature = re.sub(r'\s+[а-яА-Я]\s+', ' ', processed_feature)

            # Remove single characters from the start
            processed_feature = re.sub(r'\^[а-яА-Я]\s+', ' ', processed_feature)

            # Substituting multiple spaces with single space
            processed_feature = re.sub(r'\s+', ' ', processed_feature, flags=re.I)

            # Removing prefixed 'b'
            processed_feature = re.sub(r'^b\s+', '', processed_feature)

            # Converting to Lowercase
            processed_feature = processed_feature.lower()

            processed_features.append(processed_feature)

            if(catboost):
                # remove all non russian words
                processed_feature = re.sub("[^А-Яа-я]", " ", processed_feature)

        return processed_features


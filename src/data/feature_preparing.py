"""Module preparing data"""
import click
from pandas import read_csv, concat


@click.command()
@click.argument("positive_data_csv", type=click.Path(exists=True))
@click.argument("negative_data_csv", type=click.Path(exists=True))
@click.argument("feature_csv")
@click.argument("labels_csv")
def feature_preparing(
        positive_data_csv: str,
        negative_data_csv: str,
        feature_csv: str,
        labels_csv: str,
):
    """
    The method receives and performs data preprocessing
    :param str positive_data_csv: path to data file with positive messages
    :param str negative_data_csv: path to data file with negative messages
    :param str feature_csv: path to prepared features
    :param str labels_csv: path to prepared labels
    :return: created features.csv and labels.csv
    """
    data1 = read_csv(positive_data_csv, delimiter=";", header=None, nrows=100)
    # nrows=100000
    data1 = data1[[3, 4]].rename({3: "x", 4: "y"}, axis="columns")
    data2 = read_csv(negative_data_csv, delimiter=";", header=None, nrows=100)
    # nrows=100000
    data2 = data2[[3, 4]].rename({3: "x", 4: "y"}, axis="columns")
    data = concat([data1, data2])
    data = data[["x", "y"]]
    features = data["x"]
    labels = data["y"]
    features.to_csv(feature_csv)
    labels.to_csv(labels_csv)
    print("Features and labels extracted successfully")


if __name__ == "__main__":
    feature_preparing(
        "data/raw/positive.csv",
        "data/raw/negative.csv",
        "data/interim/features.csv",
        "data/processed/labels.csv",

    )

    # r"C:\Users\mpoin\OneDrive\Documents\Projects"
    # r"\emotion-recognition-chatbot\data\raw\positive.csv",
    # r"C:\Users\mpoin\OneDrive\Documents\Projects"
    # r"\emotion-recognition-chatbot\data\raw\negative.csv",
    # r"C:\Users\mpoin\OneDrive\Documents\Projects"
    # r"\emotion-recognition-chatbot\data\interim/features.csv",
    # r"C:\Users\mpoin\OneDrive\Documents\Projects"
    # r"\emotion-recognition-chatbot\data\processed/labels.csv",

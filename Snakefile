rule  all:
    input:
        "data/processed/processed_features.csv",
        "data/interim/features.csv",
        "data/processed/labels.csv"

rule feature_processing:
    input:
        "data/interim/features.csv"
    output:
        "data/processed/processed_features.csv"
    shell:
        "python -m src.data.feature_processing {input} {output}"


rule feature_extracting:
    input:
        "data/raw/positive.csv",
        "data/raw/negative.csv"
    output:
        "data/interim/features.csv",
        "data/processed/labels.csv"
    shell:
        "python -m src.data.feature_preparing {input[0]} {input[1]} {output[0]} {output[1]}"

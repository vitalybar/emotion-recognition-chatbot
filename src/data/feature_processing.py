"""Module processing data"""
import re
import click
from pandas import read_csv, DataFrame


@click.command()
@click.argument("features_csv", type=click.Path(exists=True))
@click.argument("processed_features_csv", type=click.Path)
def feature_processing(
    features_csv: str,
    processed_features_csv: str,
):
    """
    The method preparing and processing features
    :param features_csv: path to features csv file
    :param processed_features_csv: path to processed features csv file
    :return: created processed_features.csv
    """
    features = read_csv(features_csv).values
    processed_features = []

    for sentence in enumerate(features):
        # Remove all the special characters
        processed_feature = re.sub(r"\W", " ", str(sentence))

        # remove all single characters
        processed_feature = re.sub(r"\s+[а-яА-Я]\s+", " ", processed_feature)

        # remove all non russian words
        processed_feature = re.sub("[^А-Яа-я]", " ", processed_feature)

        # Remove single characters from the start
        processed_feature = re.sub(r"\^[а-яА-Я]\s+", " ", processed_feature)

        # Substituting multiple spaces with single space
        processed_feature = re.sub(r"\s+", " ", processed_feature, flags=re.I)

        # Removing prefixed 'b'
        processed_feature = re.sub(r"^b\s+", "", processed_feature)

        # Converting to Lowercase
        processed_feature = processed_feature.lower()

        processed_features.append(processed_feature)

    DataFrame(processed_features).to_csv(processed_features_csv)
    print("Features processed successfully")


if __name__ == "__main__":
    feature_processing(
        "data/interim/features.csv",
        "data/processed/processed_features.csv"
    )

    # r"C:\Users\mpoin\OneDrive\Documents\Projects
    # \emotion-recognition-chatbot\data\interim\features.csv",
    # r"C:\Users\mpoin\OneDrive\Documents\Projects
    # \emotion-recognition-chatbot\data\processed\processed_features.csv"

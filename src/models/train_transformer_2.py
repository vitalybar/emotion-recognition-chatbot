"""Module training and evaluation Transformer model"""
import os
import pandas as pd
import torch
import mlflow
from mlflow.models.signature import infer_signature
from sklearn.model_selection import train_test_split
from torch.utils.data import DataLoader, Dataset
from transformers import (
    BertForSequenceClassification,
    AdamW,
    DistilBertTokenizer,
    pipeline,
)

# mlflow.set_tracking_uri("http://localhost:5000")
# os.environ["MLFLOW_S3_ENDPOINT_URL"] = "http://localhost:9000"

experiment_name = "Transformer-train"
transformer_name = "Transformer-model"
transformer_model_arts = "Transformer-train"

mlflow.set_experiment(experiment_name)
os.environ["MLFLOW_EXPERIMENT_NAME"] = experiment_name


mlflow.transformers.autolog()
with mlflow.start_run():
    processed_features = pd.read_csv(
        r"C:\Users\mpoin\OneDrive\Documents\Projects"
        r"\emotion-recognition-chatbot\data\processed"
        r"\processed_features.csv"
    )["0"].tolist()
    labels = pd.read_csv(
        r"C:\Users\mpoin\OneDrive\Documents\Projects"
        r"\emotion-recognition-chatbot\data\processed"
        r"\labels.csv"
    )
    labels.loc[labels["y"] < 0, "y"] = 0
    labels = labels["y"].tolist()

    # Разделение на тренировочную и тестовую выборки
    train_texts, val_texts, train_labels, val_labels = train_test_split(
        processed_features, labels, test_size=0.2
    )

    # Инициализация токенизатора и модели BERT
    tokenizer = DistilBertTokenizer.from_pretrained("distilbert-base-uncased")
    config = {
        "num_classes": 2,
        "dropout_rate": 0.1,
    }
    model = BertForSequenceClassification.from_pretrained("distilbert-base-uncased")

    components = {
        "model": model,
        "tokenizer": tokenizer,
    }

    # Создание класса для датасета
    class MovieReviewDataset(Dataset):
        """
        Dataset class to train and test Transformer model
        """
        def __init__(self, texts, labels_dataset, tokenizer_dataset, max_seq_len):
            self.texts = texts
            self.labels = labels_dataset
            self.tokenizer = tokenizer_dataset
            self.max_seq_len = max_seq_len

        def __len__(self):
            return len(self.texts)

        def __getitem__(self, idx):
            text = str(self.texts[idx])
            label = self.labels[idx]

            encoding = self.tokenizer.encode_plus(
                text,
                add_special_tokens=True,
                return_token_type_ids=False,
                truncation=True,
                return_attention_mask=True,
                return_tensors="pt",
                padding="max_length",
                max_length=self.max_seq_len,
            )
            return {
                "text": text,
                "input_ids": encoding["input_ids"].flatten(),
                "attention_mask": encoding["attention_mask"].flatten(),
                "label": torch.tensor(label, dtype=torch.long),
            }

    # Создание объектов для тренировочной и тестовой выборок
    train_dataset = MovieReviewDataset(train_texts, train_labels, tokenizer, 128)
    val_dataset = MovieReviewDataset(val_texts, val_labels, tokenizer, 128)

    # Создание DataLoader'ов для пакетного обучения модели
    train_loader = DataLoader(train_dataset, batch_size=8, shuffle=True)
    val_loader = DataLoader(val_dataset, batch_size=8, shuffle=False)

    # Определение параметров обучения
    optimizer = AdamW(model.parameters(), lr=2e-5, weight_decay=1e-2)
    NUM_EPOCHS = 5

    # Обучение модели
    for epoch in range(NUM_EPOCHS):
        model.train()
        train_loss: float = 0.0
        for i, batch in enumerate(train_loader):
            input_ids = batch["input_ids"]
            attention_mask = batch["attention_mask"]
            labels = batch["label"]

            optimizer.zero_grad()
            outputs = model(
                input_ids=input_ids, attention_mask=attention_mask, labels=labels
            )
            loss = outputs[0]
            train_loss += loss.item()

            loss.backward()
            optimizer.step()

        print(f"Epoch: {epoch}, Training Loss: {train_loss / len(train_loader)}")
    mlflow.transformers.save_model(
        transformers_model=components,
        path=fr"C:\Users\mpoin\OneDrive\Documents\Projects"
        fr"\emotion-recognition-chatbot\models\{transformer_name}",
    )

    # Оценка модели на тестовой выборке
    model.eval()
    with torch.no_grad():
        val_preds = []
        for batch in val_loader:
            input_ids = batch["input_ids"]
            attention_mask = batch["attention_mask"]

            outputs = model(input_ids=input_ids, attention_mask=attention_mask)
            logits = outputs.logits
            preds = torch.argmax(logits, dim=-1)
            val_preds.extend(preds.cpu().detach().numpy().tolist())

    # metrics = model.eval()
    # mlflow.log_metrics(metrics)
    val_preds = pd.DataFrame(val_preds).astype("float")
    signature = infer_signature(train_texts, val_preds)

    transformers_model = pipeline(
        task="text-classification", tokenizer=tokenizer, model=model
    )
    ARTIFACT_PATH = (
        rf"C:\Users\mpoin\OneDrive\Documents\Projects"
        rf"\emotion-recognition-chatbot\models\{transformer_model_arts}"
    )
    mlflow.transformers.log_model(
        transformers_model=transformers_model,
        artifact_path=ARTIFACT_PATH,
        signature=signature,
    )

    autolog_run = mlflow.last_active_run()
    mlflow.end_run()
